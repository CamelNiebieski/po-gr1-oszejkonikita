package com.company;
import java.util.Random;
import java.util.Scanner;

public class Main2 {

    public static void main(String[] args) {

        Scanner podaj = new Scanner(System.in);
        System.out.println("Podaj n: ");

        int n = podaj.nextInt();

        int[] tab = new int[n];
        Random rand = new Random();

        int parzyste = 0;
        int nieparzyste = 0;
        int ujemne = 0;
        int sumaUj = 0;
        int dodatnie = 0;
        int sumaDod = 0;
        int zerowe = 0;
        int max = -1000;
        int iloscMaks = 0;
        int maksDl = 0;
        int tempDl = 0;
        for(int i = 0; i < n; i++){
            tab[i] = rand.nextInt(1999) - 999;
            /*a*/
            if(tab[i] % 2 == 0) parzyste++;
            else if(tab[i] % 2 != 0) nieparzyste++;

            /*b*/
            if(tab[i] > 0) {dodatnie++; sumaDod += tab[i];}
            else if(tab[i] < 0) {ujemne++; sumaUj += tab[i];}
            else zerowe++;

            /*c*/
            if(max < tab[i]){
                max = tab[i];
                iloscMaks = 1;
            }
            else iloscMaks++;
        }
    }
}
