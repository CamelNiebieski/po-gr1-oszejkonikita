package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    int suma = 0;

        System.out.println("Wprowadź n: ");
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

	    for(int i = 1; i <= n; i++){
            int a = scan.nextInt();
	        suma += a;
        }
        System.out.println("Wynik sumowania: " + suma);

        suma = 1;

        System.out.println("Wprowadź n: ");
        n = scan.nextInt();

        for(int i = 1; i <= n; i++){
            int a = scan.nextInt();
            suma *= a;
        }
        System.out.println("Wynik mnozenia: " + suma);

        suma = 0;

        System.out.println("Wprowadź n: ");
        n = scan.nextInt();

        for(int i = 1; i <= n; i++){
            int a = scan.nextInt();
            suma += Math.abs(a);
        }
        System.out.println("Wynik sumowania: " + suma);

        suma = 0;

        System.out.println("Wprowadź n: ");
        n = scan.nextInt();

        for(int i = 1; i <= n; i++){
            int a = scan.nextInt();
            suma += Math.sqrt(Math.abs(a));
        }
        System.out.println("Wynik sumowania: " + suma);

        suma = 1;

        System.out.println("Wprowadź n: ");
        n = scan.nextInt();

        for(int i = 1; i <= n; i++){
            int a = scan.nextInt();
            suma *= Math.abs(a);
        }
        System.out.println("Wynik mnozenia: " + suma);

        suma = 1;

        System.out.println("Wprowadź n: ");
        n = scan.nextInt();

        for(int i = 1; i <= n; i++){
            int a = scan.nextInt();
            suma *= (a * a);
        }
        System.out.println("Wynik mnozenia potęg: " + suma);

        suma = 1;
        int suma2 = 0;

        System.out.println("Wprowadź n: ");
        n = scan.nextInt();

        for(int i = 1; i <= n; i++){
            int a = scan.nextInt();

            suma *= a;
            suma2 += a;
        }
        System.out.println("Wynik mnozenia: " + suma + suma2);

        System.out.println("Wprowadź n: ");
        n = scan.nextInt();
        int temp = 0;
        for(int i = 1; i <= n; i++){
            int a = scan.nextInt();
            if(i == 1){
                temp = a;
            }
            else {
                System.out.println(a);
            }
        }
        System.out.println(temp);

        suma = 0;

        System.out.println("Wprowadź n: ");
        n = scan.nextInt();

        for(int i = 1; i <= n; i++){
            int a = scan.nextInt();
            if(a % 2 == 1){
                suma++;
            }
        }
        System.out.println("Nieparzyste: " + suma);

        suma = 0;

        System.out.println("Wprowadź n: ");
        n = scan.nextInt();

        for(int i = 1; i <= n; i++){
            int a = scan.nextInt();
            if(a % 3 == 0 && a % 5 != 0){
                suma++;
            }
        }
        System.out.println("Podzielne przez 3 i niepodzielne przez 5: " + suma);

        suma = 0;

        System.out.println("Wprowadź n: ");
        n = scan.nextInt();

        for(int i = 1; i <= n; i++){
            int a = scan.nextInt();
            if(Math.sqrt(a) % 2 == 0) {
                suma++;
            }

        }
        System.out.println("Kwadraty liczb parzystych: " + suma);


        suma = 0;

        System.out.println("Wprowadź n: ");
        n = scan.nextInt();

        for(int i = 1; i < n / 3; i++){
            int a = scan.nextInt();
            int b = scan.nextInt();
            int c = scan.nextInt();
            if(b < (a + c) / 2){
                suma++;
            }
        }
        System.out.println("Cos: " + suma);

        suma = 0;

        System.out.println("Wprowadź n: ");
        n = scan.nextInt();

        for(int i = 1; i <= n; i++){
            int a = scan.nextInt();
            if(a > 0){
                suma += a;
            }
        }
        System.out.println("Wynik mnozenia: " + (suma * 2));

        int dod = 0;
        int zero = 0;
        int ujem = 0;

        System.out.println("Wprowadź n: ");
        n = scan.nextInt();

        for(int i = 1; i <= n; i++){
            int a = scan.nextInt();
            if(a > 0) dod++;
            else if(a < 0) ujem++;
            else zero++;
        }
        System.out.println("Dodatkie: "+ dod);
        System.out.println("ujemne: "+ ujem);
        System.out.println("Zera: "+ zero);


        int max = 0;
        int min = 0;

        System.out.println("Wprowadź n: ");
        n = scan.nextInt();

        for(int i = 1; i <= n; i++){
            int a = scan.nextInt();
            if(a > max) max = a;
            else if(min > a) min = a;
        }
        System.out.println("Max " + max);
        System.out.println("Min: " + min);


    }
}
